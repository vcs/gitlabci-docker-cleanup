FROM alpine:latest


VOLUME ["/App"]
WORKDIR /App
ENTRYPOINT ["/run.sh"]

ENV CLEAN_PERIOD=**None** \
    DELAY_TIME=**None** \
    KEEP_IMAGES=**None** \
    KEEP_CONTAINERS=**None** \
    LOOP=true \
    DEBUG=0 \
    DOCKER_API_VERSION=1.24

# run.sh script uses some bash specific syntax
RUN apk add --update bash docker grep bc lvm2

# Install cleanup script
ADD docker-cleanup-volumes.sh /docker-cleanup-volumes.sh
ADD run.sh /run.sh

