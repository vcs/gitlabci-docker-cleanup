#!/bin/bash

free_space_percent_lower_than() {
  threshold_percent=$1
  storage_driver=$(docker info | grep -Po '(?<=Storage Driver: ).*')
  if [ "$storage_driver" = "devicemapper" ]
  then
    current_used_percent=$(lvs docker-vg/docker-pool -o data_percent --noheadings)
  elif [ "$storage_driver" = "overlay2" ]
  then
    current_used_percent=$( df -h / | awk '{ print $5 }' | tail -n 1 | tr -d '%')
  else
    # We couldn't retrieve the percentage
    echo "[$(date +'%D %T')] The storage driver could not be retrieved"
    exit 1
  fi
  if [ $DEBUG ]; then echo "[$(date +'%D %T')] DEBUG: Current docker storage usage is: ${current_used_percent} %"; fi
  test $(echo "(100 - ${current_used_percent}) < ${threshold_percent}" | bc) -eq "1"
}

checkPatterns() {
    keepit=$3
    if [ -n "$1" ]; then
        for PATTERN in $(echo $1 | tr "," "\n"); do
        if [[ "$2" = $PATTERN* ]]; then
            if [ $DEBUG ]; then echo "[$(date +'%D %T')] DEBUG: Matches $PATTERN - keeping"; fi
            keepit=1
        else
            if [ $DEBUG ]; then echo "[$(date +'%D %T')] DEBUG: No match for $PATTERN"; fi
        fi
        done
    fi
    return $keepit
}

checkAge() {
    min_age=$1
    date_str=$2
    keepit=$3
    # Docker timestamp looks like 2017-08-10T13:45:32.916018668Z
    # We must also take BusyBox's date command specificities into account
    date_epoch=$(date -d "$(echo ${date_str} | cut -d. -f1)" -D '%Y-%m-%dT%T' +%s)
    now_epoch=$(date +%s)
    age=$(expr ${now_epoch} - ${date_epoch})
    if [ "${age}" -lt "${min_age}" ]; then
        if [ $DEBUG ]; then echo "[$(date +'%D %T')] DEBUG: age is $age seconds, less than $min_age - keeping"; fi
        keepit=1
    else
        if [ $DEBUG ]; then echo "[$(date +'%D %T')] DEBUG: age is $age seconds, greater than $min_age"; fi
    fi
    return $keepit
}


if [ ! -e "/var/run/docker.sock" ]; then
    echo "[$(date +'%D %T')] => Cannot find docker socket(/var/run/docker.sock), please check the command!"
    exit 1
fi

if docker version >/dev/null; then
    echo "[$(date +'%D %T')] docker is running properly"
else
    echo "[$(date +'%D %T')] Cannot run docker binary at /usr/bin/docker"
    echo "[$(date +'%D %T')] Please check if the docker binary is mounted correctly"
    exit 1
fi

# start deleting images when we reach that percentage of free docker storage
LOW_FREE_SPACE_PERCENT=${LOW_FREE_SPACE_PERCENT:-25}
# delete images until we get to that percentage of docker storage
EXPECTED_FREE_SPACE_PERCENT=${EXPECTED_FREE_SPACE_PERCENT:-50}
# keep exited containers at least that amount of time (in seconds)
# NB: GitLab-CI uses stopped helper containers to check out project code and retrieve artifacts after build completes
# so a job that generates artifacts will fail if it runs for longer than the specified period, as the stopped helper container
# used to retrieve artifacts will have been deleted.
# cf. https://gitlab.com/gitlab-org/gitlab-ci-multi-runner/issues/2347
MIN_EXITED_CONTAINER_AGE=${MIN_EXITED_CONTAINER_AGE:-172800} # Use 48 hours by default

if [ "${CLEAN_PERIOD}" == "**None**" ]; then
    echo "[$(date +'%D %T')] => CLEAN_PERIOD not defined, use the default value."
    CLEAN_PERIOD=1800
fi

if [ "${DELAY_TIME}" == "**None**" ]; then
    echo "[$(date +'%D %T')] => DELAY_TIME not defined, use the default value."
    DELAY_TIME=1800
fi

if [ "${KEEP_IMAGES}" == "**None**" ]; then
    unset KEEP_IMAGES
fi

if [ "${KEEP_CONTAINERS}" == "**None**" ]; then
    unset KEEP_CONTAINERS
fi
if [ "${KEEP_CONTAINERS}" == "**All**" ]; then
    KEEP_CONTAINERS="."
fi

if [ "${KEEP_CONTAINERS_NAMED}" == "**None**" ]; then
    unset KEEP_CONTAINERS_NAMED
fi
if [ "${KEEP_CONTAINERS_NAMED}" == "**All**" ]; then
    KEEP_CONTAINERS_NAMED="."
fi

if [ "${LOOP}" != "false" ]; then
    LOOP=true
fi

if [ "${DEBUG}" == "0" ]; then
    unset DEBUG
fi

if [ $DEBUG ]; then echo DEBUG ENABLED; fi

echo "[$(date +'%D %T')] => Run the clean script every ${CLEAN_PERIOD} seconds and delay ${DELAY_TIME} seconds to clean."

trap '{ echo "[$(date +'%D %T')] User Interupt."; exit 1; }' SIGINT
trap '{ echo "[$(date +'%D %T')] SIGTERM received, exiting."; exit 0; }' SIGTERM
while [ 1 ]
do
    if [ $DEBUG ]; then echo DEBUG: Starting loop; fi

    # Cleanup unused volumes

    if [[ $(docker version --format '{{(index .Server.Version)}}' | grep -E '^[01]\.[012345678]\.') ]]; then
      echo "[$(date +'%D %T')] => Removing unused volumes using 'docker-cleanup-volumes.sh' script"
      /docker-cleanup-volumes.sh
    else
      echo "[$(date +'%D %T')] => Removing unused volumes using native 'docker volume' command"
      for volume in $(docker volume ls -qf dangling=true); do
        echo "[$(date +'%D %T')] Deleting volume ${volume}"
        docker volume rm "${volume}"
      done
    fi

    IFS='
 '

    # Cleanup exited/dead containers
    echo "[$(date +'%D %T')] => Removing exited/dead containers"
    rm -f ExitedContainerIdList
    touch ExitedContainerIdList
    EXITED_CONTAINERS_IDS="`docker ps -a -q -f status=exited -f status=dead | xargs echo`"
    for CONTAINER_ID in $EXITED_CONTAINERS_IDS; do
      CONTAINER_IMAGE=$(docker inspect --format='{{(index .Config.Image)}}' $CONTAINER_ID)
      CONTAINER_NAME=$(docker inspect --format='{{(index .Name)}}' $CONTAINER_ID)
      CONTAINER_CREATED=$(docker inspect --format='{{(index .Created)}}' $CONTAINER_ID)
      if [ $DEBUG ]; then echo "[$(date +'%D %T')] DEBUG: Check container image $CONTAINER_IMAGE named $CONTAINER_NAME created $CONTAINER_CREATED"; fi
      keepit=0
      checkPatterns "${KEEP_CONTAINERS}" "${CONTAINER_IMAGE}" $keepit
      keepit=$?
      checkPatterns "${KEEP_CONTAINERS_NAMED}" "${CONTAINER_NAME}" $keepit
      keepit=$?
      checkAge "${MIN_EXITED_CONTAINER_AGE}" "${CONTAINER_CREATED}" $keepit
      keepit=$?
      if [[ $keepit -eq 0 ]]; then
        echo "[$(date +'%D %T')] Will remove stopped container $CONTAINER_ID"
        echo "${CONTAINER_ID}" >> ExitedContainerIdList
      fi
    done
    unset CONTAINER_ID

    echo "[$(date +'%D %T')] => Removing unused images"

    # Get all containers in "created" state
    rm -f CreatedContainerIdList
    docker ps -a -q -f status=created | sort > CreatedContainerIdList

    # Get all image ID
    ALL_LAYER_NUM=$(docker images -a | tail -n +2 | wc -l)
    docker images -q --no-trunc | sort -o ImageIdList
    CONTAINER_ID_LIST=$(docker ps -aq --no-trunc)
    # Get Image ID that is used by a containter
    rm -f ContainerImageIdList
    touch ContainerImageIdList
    for CONTAINER_ID in ${CONTAINER_ID_LIST}; do
        LINE=$(docker inspect ${CONTAINER_ID} | grep "\"Image\": \"\(sha256:\)\?[0-9a-fA-F]\{64\}\"")
        IMAGE_ID=$(echo ${LINE} | awk -F '"' '{print $4}')
        echo "${IMAGE_ID}" >> ContainerImageIdList
    done
    sort ContainerImageIdList -o ContainerImageIdList

    # Remove the images being used by containers from the delete list
    comm -23 ImageIdList ContainerImageIdList > ToBeCleanedImageIdList

    # Remove those reserved images from the delete list
    if [ -n "${KEEP_IMAGES}" ]; then
      rm -f KeepImageIdList
      touch KeepImageIdList
      # This looks to see if anything matches the regexp
      docker images --no-trunc | (
        while read repo tag image junk; do
          keepit=0
          if [ $DEBUG ]; then echo "[$(date +'%D %T')] DEBUG: Check image $repo:$tag"; fi
          for PATTERN in $(echo ${KEEP_IMAGES} | tr "," "\n"); do
            if [[ -n "$PATTERN" && "${repo}:${tag}" = $PATTERN* ]]; then
              if [ $DEBUG ]; then echo "[$(date +'%D %T')] DEBUG: Matches $PATTERN"; fi
              keepit=1
            else
              if [ $DEBUG ]; then echo "[$(date +'%D %T')] DEBUG: No match for $PATTERN"; fi
            fi
          done
          if [[ $keepit -eq 1 ]]; then
            if [ $DEBUG ]; then echo "[$(date +'%D %T')] DEBUG: Marking image $repo:$tag to keep"; fi
            echo $image >> KeepImageIdList
          fi
        done
      )
      # This explicitly looks for the images specified
      arr=$(echo ${KEEP_IMAGES} | tr "," "\n")
      for x in $arr
      do
          if [ $DEBUG ]; then echo "[$(date +'%D %T')] DEBUG: Identifying image $x"; fi
          docker inspect $x 2>/dev/null| grep "\"Id\": \"\(sha256:\)\?[0-9a-fA-F]\{64\}\"" | head -1 | awk -F '"' '{print $4}'  >> KeepImageIdList
      done
      sort KeepImageIdList -o KeepImageIdList
      comm -23 ToBeCleanedImageIdList KeepImageIdList > ToBeCleanedImageIdList2
      mv ToBeCleanedImageIdList2 ToBeCleanedImageIdList
    fi

    # Wait before cleaning containers and images
    echo "[$(date +'%D %T')] => Waiting ${DELAY_TIME} seconds before cleaning"
    sleep ${DELAY_TIME} & wait

    # Remove created containers that haven't managed to start within the DELAY_TIME interval
    rm -f CreatedContainerToClean
    comm -12 CreatedContainerIdList <(docker ps -a -q -f status=created | sort) > CreatedContainerToClean
    if [ -s CreatedContainerToClean ]; then
        echo "[$(date +'%D %T')] => Start to clean $(cat CreatedContainerToClean | wc -l) created/stuck containers"
        if [ $DEBUG ]; then echo "[$(date +'%D %T')] DEBUG: Removing unstarted containers"; fi
        docker rm -v $(cat CreatedContainerToClean)
    fi

    if [ -s ExitedContainerIdList ]; then
        echo "[$(date +'%D %T')] => Start to clean $(cat ExitedContainerIdList | wc -l) exited/dead containers"
        docker rm -v $(cat ExitedContainerIdList)
    fi

    # Remove images being used by containers from the delete list again. This prevents the images being pulled from deleting
    CONTAINER_ID_LIST=$(docker ps -aq --no-trunc)
    rm -f ContainerImageIdList
    touch ContainerImageIdList
    for CONTAINER_ID in ${CONTAINER_ID_LIST}; do
        LINE=$(docker inspect ${CONTAINER_ID} | grep "\"Image\": \"\(sha256:\)\?[0-9a-fA-F]\{64\}\"")
        IMAGE_ID=$(echo ${LINE} | awk -F '"' '{print $4}')
        echo "${IMAGE_ID}" >> ContainerImageIdList
    done
    sort ContainerImageIdList -o ContainerImageIdList
    comm -23 ToBeCleanedImageIdList ContainerImageIdList > ToBeCleaned

    # Remove Images
    if ! free_space_percent_lower_than $LOW_FREE_SPACE_PERCENT; then
        echo "[$(date +'%D %T')] There is sufficient docker storage free space, not cleaning images."
    elif [ -s ToBeCleaned ]; then
        echo "[$(date +'%D %T')] => Start to clean $(cat ToBeCleaned | wc -l) images"
        # query again the list of images from Docker so we have them ordered by most recent first, then keep only the images
        # that are in our ToBeCleaned list and revert the list so oldest is first. Then delete them one by one until we
        # have enough free space.
        docker images --no-trunc --format='{{.ID}}' | uniq | grep -x -f ToBeCleaned | tac | while read i; do
            docker rmi $i
            # stop once we've reclaimed enough free space
            free_space_percent_lower_than $EXPECTED_FREE_SPACE_PERCENT || break
        done;
        #docker rmi $(cat ToBeCleaned) 2>/dev/null
        (( DIFF_LAYER=${ALL_LAYER_NUM}- $(docker images -a | tail -n +2 | wc -l) ))
        (( DIFF_IMG=$(cat ImageIdList | wc -l) - $(docker images | tail -n +2 | wc -l) ))
        if [ ! ${DIFF_LAYER} -gt 0 ]; then
                DIFF_LAYER=0
        fi
        if [ ! ${DIFF_IMG} -gt 0 ]; then
                DIFF_IMG=0
        fi
        echo "[$(date +'%D %T')] => Done! ${DIFF_IMG} images and ${DIFF_LAYER} layers have been cleaned."
    else
        echo "[$(date +'%D %T')] No images need to be cleaned"
    fi

    rm -f ToBeCleanedImageIdList ContainerImageIdList ToBeCleaned ImageIdList KeepImageIdList

    # Run forever or exit after the first run depending on the value of $LOOP
    [ "${LOOP}" == "true" ] || break

    echo "[$(date +'%D %T')] => Next clean will be started in ${CLEAN_PERIOD} seconds"
    sleep ${CLEAN_PERIOD} & wait
done